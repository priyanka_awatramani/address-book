<?php
function db_connect()
{
    static $connection;
    
    if(!$connection)
    {
        //try to connect as database connection is not established
        $config = parse_ini_file('config.ini'); //here we have not written includes/config.ini bcz jab index me require karenge to it will be used in root folder only
        $connection = mysqli_connect($config['host'], $config['username'], $config['password'], $config['dbname']);
    }
    
    //i assure you that $connection has a valid connection established
    if($connection === false)
    {
        return mysqli_connect_error();
    }
    return $connection;
}

function db_query($query)
{
    $connection = db_connect();
    
    $result = mysqli_query($connection, $query);
    
    return $result;
}

function db_error()
{
    $connection = db_connect();
    return mysqli_error($connection);
}

function db_select($query)
{
    $result = db_query($query);
    if($result === false)
    {
        return false;
    }
    $rows = array();
    while($row = mysqli_fetch_assoc($result))
    {
        $rows[] = $row;
    }
    
    return $rows;
}

function db_quote($value)
{
    $connection = db_connect();
    return mysqli_real_escape_string($connection, $value);
}

function add_single_quotes($variable){
    return "'$variable'";
}

function dd($variable)
{
    die(var_dump($variable));
}



function redirect($url){
    header("location: $url");
}









